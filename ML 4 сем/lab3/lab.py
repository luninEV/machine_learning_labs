import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), '..', 'common'))
import lab_common as lab

import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split

def train_conv(x_train, x_test, y_train, y_test, epochs,img_height,img_width):
    model=tf.keras.models.Sequential([
        tf.keras.layers.Conv2D(8,(3,3),activation='relu',input_shape=(img_height, img_width,1)),
        tf.keras.layers.Conv2D(16,(3,3),activation='relu'),
        tf.keras.layers.Flatten(),        
        tf.keras.layers.Dense(128, activation='relu'),
        tf.keras.layers.Dense(10, activation='softmax')
    ])

    model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])
    
    model.fit(x_train, y_train, epochs=epochs)
    model.evaluate(x_test, y_test)

def train_conv_pool(x_train, x_test, y_train, y_test, epochs,img_height,img_width):
    model=tf.keras.models.Sequential([
        tf.keras.layers.Conv2D(8,(3,3),activation='relu',input_shape=(img_height, img_width,1)),
        tf.keras.layers.AveragePooling2D(),
        tf.keras.layers.Flatten(),        
        tf.keras.layers.Dense(128, activation='relu'),
        tf.keras.layers.Dense(10, activation='softmax')
    ])

    model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])
    
    model.fit(x_train, y_train, epochs=epochs)
    model.evaluate(x_test, y_test)

def train_LeNet5(x_train, x_test, y_train, y_test, epochs):
    model=tf.keras.models.Sequential([
        tf.keras.layers.Conv2D(filters=6, kernel_size=(5, 5), activation='relu', input_shape=(32,32,1)),
        tf.keras.layers.AveragePooling2D(),
        tf.keras.layers.Conv2D(filters=16, kernel_size=(5, 5), activation='relu'),
        tf.keras.layers.AveragePooling2D(),
        tf.keras.layers.Flatten(),        
        tf.keras.layers.Dense(120, activation='relu'),
        tf.keras.layers.Dense(84, activation='relu'),
        tf.keras.layers.Dense(10, activation='softmax')
    ])

    model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])
    
    model.fit(x_train, y_train, epochs=epochs)
    model.evaluate(x_test, y_test)

def add_pad(X,pad):
    return np.pad(X,((0,0),(pad,pad),(pad,pad),(0,0)),mode='constant', constant_values=(0,0))

def run():
    #dataset_url="https://commondatastorage.googleapis.com/books1000/notMNIST_large.tar.gz"
    dataset_url="https://commondatastorage.googleapis.com/books1000/notMNIST_small.tar.gz"
    img_height=img_width=28
    classes=["A","B","C","D","E","F","G","H","I","J"]
    epochs=100
    pad=2

    dataset_root_path=lab.extract_dataset(lab.download_dataset(dataset_url))
    hdf5_path=lab.create_hdf5(dataset_root_path,classes,img_height,img_width)
    X,Y=lab.read_hdf5(hdf5_path)
    X=np.reshape(X,(X.shape[0], X.shape[1], X.shape[2],1))
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)
    train_conv(X_train, X_test, Y_train, Y_test, epochs,img_height,img_width)
    train_conv_pool(X_train, X_test, Y_train, Y_test, epochs,img_height,img_width)
    X_train=add_pad(X_train,pad)
    X_test=add_pad(X_test,pad)
    train_LeNet5(X_train, X_test, Y_train, Y_test, epochs)

if __name__ == "__main__":
    run()